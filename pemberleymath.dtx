\documentclass{ltxdoc}
\usepackage{pemberleymath}
\title{The PemberleyMath Package}
\author{Adrian Rettich}
\email{adrian.rettich@gmail.com}
\begin{document}


\maketitle
\section{Synopsis}
  The PemberleyMath package loads some useful packages and provides a slew of commands I need often.

  If you want to load this package for some of its new macros without changing anything about code you have already written, simply load it with the \emph{newmacrosonly} option.

  \section{Loaded Packages}
  PemberleyMath loads \textbf{pemberley}, \textbf{amsmath}, \textbf{amssymb}, \textbf{amsfonts}, \textbf{mathtools}, \textbf{mathdots}, \textbf{faktor}, and \textbf{braket}.

  \section{Formatting}

  By default, PemberleyMath shows numbers on only those equations that need them, i.e.\ those that are referenced somewhere in your document. You can suppress this behavior (such that all equations are numbered) by passing the \emph{equationnumbers} option.

   % \todo{explanation about boldmath}

   Finally, PemberleyMath swaps the commands \textbackslash phi and \textbackslash varphi, such that the former now produces $\phi$ and the latter produces $\varphi$. You can keep the original commands (though why you would want to escapes me) by passing the \emph{keepphi} option. The same goes for the \textbackslash epsilon (now produces $\epsilon$) and \textbackslash varepsilon (now $\varepsilon$) commands together with the \emph{keepepsilon} option.

  \section{Macros}

  PemberleyMath provides the following additional macros.

  \subsection{Number Spaces}

  \begin{description}
	  \item[\textbackslash NN, \textbackslash ZZ, \textbackslash QQ, \textbackslash RR, \textbackslash CC] Aliases for \textbackslash mathbb\{letter\}, i.e.\ $\NN, \ZZ, \QQ, \RR, \CC$. These can be used without manually entering math mode.
  \end{description}
\newcommand\bb\textbackslash
  \subsection{Math Operators}
  Note that these can only be used in math mode.
  \begin{description}
	  \item[\bb Hom, \bb Obj, \bb id] Give $\Hom, \Obj,$ and $\id$, respectively.
	  \item[\bb ker, \bb im, \bb coker, \bb coim] Kernel, image, cokernel and coimage. These override the default behavior by shrinking the space before the next character to make parens-less notation look a bit nicer, as in $\ker f$, $\im g^2$. You may want to add a \bb thinspace when using parens or chaining operators, as in $\coker\thinspace\coim\thinspace(h)$.
  \end{description}

  \subsection{Sets}
  The \textbf{braket} package provides a wonderful option for typesetting sets in math mode.
  \begin{description}
	  \item [\bb set\{x $\mid$ x\bb in \bb RR\}] Gives $\set{x|x\in\RR}$.
  \end{description}
  This is the same as the \bb Set command in \textbf{braket}. Note that in Pemberley, the command is spelled with a lowercase s because \bb Set is reserved for the category $\Set$. To suppress this change and retain the original \bb set and \bb Set commands from \textbf{braket}, load PemberleyMath with the option \emph{keepset}.

  Furthermore, PemberleyMath provides macros for standard intervals (you can put whatever you want in place of ``0,1''):
  \begin{description}
	  \item[\bb intervaloo\{0,1\}] The open interval $\intervaloo{0,1}$.
	  \item[\bb intervalcc\{0,1\}] The closed interval $\intervalcc{0,1}$.
	  \item[\bb intervaloc\{0,1\}] The half open interval $\intervaloc{0,1}$.
	  \item[\bb intervalco\{0,1\}] The half open interval $\intervalco{0,1}$.
  \end{description}

  Finally:

  \begin{description}
      \item[\textbackslash emptyset] Is changed to mean $\emptyset$ rather than $\uglyemptyset$. The latter is still accessible as \textbackslash uglyemptyset, and you can disable the new behavior by loading PemberleyMath with the \emph{keepemptyset} option.
      \item[\textbackslash uglyemptyset] Produces $\uglyemptyset$.
  \end{description}

  \subsection{Categories}
  I like typesetting category names in smallcaps. A few are provided by default: \bb Grp, \bb Ab, \bb Ring, \bb Rng, \bb Top, and \bb Pair produce \Grp, \Ab, \Ring, \Rng, \Top, \Pair, respectively. There is no need to change to math mode. \bb Set produces \Set, unless you loaded PemberleyMath with the \emph{keepset} option.

  You can use \bb cat\{\#1\} to typeset categories not in the above list: \bb cat\{Foo\} produces \cat{Foo}.

  Finally, some categories take arguments:

  \begin{description}
	  \item [\bb Mod\{R\}] produces $\Mod R$ for the category of $R$-modules.
	  \item[\bb pair\{X\}\{Y\}] produces $\pair{X}{Y}$.
	  \item[\bb Ch\{\bb Ab\}] produces $\Ch\Ab$ for the category of chain complexes over $\Ab$.
	  \item[\bb CoCh\{\bb Ab\}] produces $\CoCh\Ab$ for cochain complexes.
  \end{description}

  \subsection{Formal Logic}
  \begin{description}
	  \item[\bb iff] Produces $\iff$, which is shorter than the standard $\Longleftrightarrow$.
	  \item[\bb implies] Produces $\implies$, which is shorter than the standard $\Longrightarrow$.
	  \item[\bb follows] Produces the corresponding $\follows$.
	  \item[\bb land] Produces $\land$, which to me looks a lot nicer than the default $\wedge$. You can still access the latter as \bb wedge.
	  \item[\bb lor] Produces $\lor$, which to me looks a lot nicer than the default $\vee$. You can still access the latter as \bb vee.
	  \item[\bb eu] Stands for ``exists uniquely'' and produces $\eu$.
  \end{description}

  \subsection{Maps and Relations}
  \begin{description}
	\item[\bb definedas] Produces $\definedas$.
	\item[\bb iso] I like to use \iso{} to mean ``isomorphic to''.
	\item[\bb homot] Likewise, $\homot$ would be ``homotopic to''.
	\item[\bb subset] Pemberley changes this command to mean $\subset$ rather than $\strictsubset$. The old symbol still exists as \bb strictsubset.
	\item[\bb strictsubset] Gives $\strictsubset$.
	\item[\bb supset] As with \textbackslash subset, Pemberley changes this command to mean $\supset$ rather than $\strictsupset$. The old symbol still exists as \bb strictsupset.
	\item[\bb strictsupset] Gives $\strictsupset$.
	\item[\bb mto, \bb eto, \bb ito] Like \bb to ($\to$), these produce arrows for maps, namely for injective (\textbf{m}onomorphisms) \mto, surjective (\textbf{e}pimorphisms) \eto, and bijective (\textbf isomorphisms) $\ito$ maps.
	\item[\bb blank] Gives a blank for function or functor definitions, as in x\bb mapsto(x,\bb blank), which renders as $x\mapsto(x,\blank)$.
  \end{description}

  \subsection{Commutative Diagrams}
  At some point I might move these to a dedicated package for typesetting commutative diagrams with tikz.

  \begin{description}
	  \item[\bb isolabel] gives you a $\isolabel$ to put over arrows.
	  \item[\bb commutes] gives you a nice little $\commutes$ to put in the center of your diagram.
  \end{description}

  \subsection{Miscellaneous Symbols}

  \begin{description}
	  \item[\bb where] Can be used to typeset constraints: \bb sum a\_i\bb where a\_i \bb in A renders as $\sum a_i\where a_i\in A$.
	  \item[\bb powerset\{set\}] Nicely typesets the power set of a set, as in $\powerset\RR$. This is slightly different from simply putting a \bb mathcal\{P\} in front of your set, as the latter results in bad spacing: $\mathcal P\RR$.
	  \item[\bb indexify\{\#1\}, \bb exponentify\{\#1\}] You can use these to make your indices and exponents smaller under specific circumstances. For example, \bb RR\^\ 1 ($\RR^1$) looks fine, but \bb RR\_\{\bb geq 1\} ($\RR_{\geq 1}$) really should be $\RR_{\indexify{\geq\negthinspace 1}}$. The latter is achieved via \bb RR\_\{\bb indexify\{\bb geq\bb negthinspace 1\}\}. \bb exponentify works the same way, but for exponents.
	  \item[\bb abs\{foo\}] Nicely sets the absolute value, as in $x=\abs{foo}$.
	  \item[\bb inv] The inverse of something: f\bb inv produces $f\inv$.
	  \item[\bb transp] The transpose: a\bb transp produces $a\transp$.
  \end{description}


  \section{Options}\label{secOptions}

  PemberleyMath accepts the same options as the base Pemberley package (with the same effects), as well as the following additional options:

  \begin{description}
      \item[newmacrosonly] Do not redefine any macros, and do not change any formatting.
	  \item[keepset] Do not redefine \bb set and \bb Set. Instead, keep the versions from \textbf{braket}.
	  \item[keepphi] Do not swap \bb phi and \bb varphi.
	  \item[keepepsilon] Do not swap \bb epsilon and \bb varepsilon.
      \item[keepemptyset] Do not renew the \textbackslash emptyset macro.
	 % \item[noparboldmath] ??
	  \item[equationnumbers] Number  \emph{all} equations, not just those you reference somewhere.
		 \end{description}


\section{License}
\coffeeware

\end{document}
