\contentsline {section}{\numberline {1}Synopsis}{1}{section.1}
\contentsline {section}{\numberline {2}Loaded Packages}{1}{section.2}
\contentsline {section}{\numberline {3}Colors}{1}{section.3}
\contentsline {section}{\numberline {4}Fonts}{2}{section.4}
\contentsline {section}{\numberline {5}Macros}{2}{section.5}
\contentsline {subsection}{\numberline {5.1}\textbackslash tuklogobox\{\emph {placement}\}}{4}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}\textbackslash tukdeptbox}{5}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}\textbackslash tukheaderbox\{\emph {text}\}[\emph {more text}]}{5}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}\textbackslash tukbox}{6}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}\textbackslash tukbox*}{7}{subsection.5.5}
\contentsline {section}{\numberline {6}Additional Settings for TUKboxes}{8}{section.6}
\contentsline {section}{\numberline {7}Options}{8}{section.7}
\contentsline {section}{\numberline {8}Installing the Fonts}{9}{section.8}
\contentsline {subsection}{\numberline {8.1}Linux}{9}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}MacOS}{9}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Windows}{9}{subsection.8.3}
\contentsline {section}{\numberline {9}License}{9}{section.9}
